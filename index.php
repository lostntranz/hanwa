<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>LeGrand High School Solar System</title>
<link href="boilerplate.css" rel="stylesheet" type="text/css">
<link href="css/mainlayout.css" rel="stylesheet" type="text/css">
<link href="css/typography.css" rel="stylesheet" type="text/css">
<!-- 
To learn more about the conditional comments around the html tags at the top of the file:
paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/

Do the following if you're using your customized build of modernizr (http://www.modernizr.com/):
* insert the link to your js here
* remove the link below to the html5shiv
* add the "no-js" class to the html tags at the top
* you can also remove the link to respond.min.js if you included the MQ Polyfill in your modernizr build 
-->
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="respond.min.js"></script>
</head>
<body>
<?php

// Main xml data parser using libxml
// For list of customizable values, see config.php
//Variable for the Solar System Size Below
/*<?php echo ($system_size / 1000) ?>kW-DC*/

require("parser.php");

?>

<div class="gridContainer clearfix">
  <div id="LayoutDiv1"><img src="images/mainheader.gif" class="headerimage"></div>
<div id="menu">
	<?php include("menuinclude.php"); ?>
</div>
<div id="mainleftpanel"><img src="images/mainleftpic.gif" class="imgdisplayed">
	<table align="center">
    	<tr>
        	<td rowspan="3">
            <img src="images/hsea-logo.gif">
            </td>
        </tr>
        <tr>
        	<td>&nbsp;
            	
            </td>
            <td>
            Solar System Size
            </td>
        </tr>
        <tr>
        	<td>&nbsp;
            	
            </td>
            <td>
            260.85 kW-DC
            </td>
        </tr>
    </table>
</div>
<div id="mainmiddlepanel">
	<table width="100%" cellpadding="15%">
    	<tr>
            	<td class="mainpagelargetext">
                Current Sun (Solar) Energy Generation (Kilowatts):
                </td>
                <td class="dynamicdatatextlarge">
                <?php echo round(($current_system_output), 2) ?> 
                </td>
                <td class="mainpagelargetext">
                kW
                </td>
            </tr>
            <tr>
            	<td class="mainpagelargetext">
                Energy Generated today (Kilowatt Hour):
                </td>
                <td class="dynamicdatatextlarge">
                <?php echo round(($current_energy_generated), 2) ?> 
                </td>
                <td class="mainpagelargetext">
                kWh
                </td>
            </tr>
            <tr>
            	<td class="mainpagelargetext">
                Energy Generated Yesterday (Kilowatt Hour):
                </td>
                <td class="dynamicdatatextlarge">
                <?php echo round(($yesterday_total), 2) ?> 
                </td>
                <td class="mainpagelargetext">
                kWh
                </td>
            </tr>
            <tr>
            	<td class="mainpagelargetext">
                Energy Generated Lifetime (Kilowatt Hour):
                </td>
                <td class="dynamicdatatextlarge">
                <?php echo round(($lifetime_energy_generated),0) ?> 
                </td>
                <td class="mainpagelargetext">
                kWh
                </td>
            </tr>
    </table>
</div>
<div id="mainrightpanel">
	<h2>Solar Resources</h2>
    <?php include("linksinclude.php"); ?>
</div>

<div id="footer"><?php include("footer.php"); ?></div>
</div>
</body>
</html>

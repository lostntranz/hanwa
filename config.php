<?php

/* Account specific parameter

American Union Elementary = 'au';
Planada Elementary School = 'pes';
Cesar Chavez Middle School = 'ccms';
LeGrand High School = 'legrand';

*/

// Set client here; see reference above.
$client = "legrand";						

// Listing client specific IDs
//
// System ID
$sysid = array( "au" => "5902",
                "pes" => "8102",
                "ccms" => "8101",
                "legrand" => "9410" );

// Datasource ID
$power1_dsid = array( "au" => "04C05B801229",
                       "pes" => "04C05B802466",
                       "ccms" => "TBD",
                       "legrand" => "04C05B802744" );
$power2_dsid = array( "au" => "04C05B8011FA",
                       "pes" => "04C05B8023F6",
                       "ccms" => "TBD",
                       "legrand" => "04C05B80320C" );

//Object ID
$temp_oid = array( "au" => "456389",
                       "pes" => "80835341",
                       "ccms" => "TBD",
                       "legrand" => "TBD" );
$graph_oid = array( "au" => "464103",
                       #"pes" => "80834873",
                       "pes" => "TBD",
                       "ccms" => "TBD",
                       "legrand" => "80841712,80841713" );

?>

<?php

/* 

##Do not make any changes here!!##

Main xml data parser using libxml. This present variables for HTML echo
For list of customizable values, see config.php
Requires connect.php for actual API access. 

*/

require("connect.php");

// Start function declaration 

function get_list_data() {
    // Getting data from remote site using 'cmd=list'
    $key = "list";
    $return_list_data = initiate_api($key);
    return $return_list_data;
}

function get_temp_data() {
    $key = "temp";
    $return_temp_data = initiate_api($key);
    //$temp_data = simplexml_load_file($return_temp_data) or die ("Unable to load temp xml string!");
    $system_temp = $temp_data->Object->Last;
    return $system_temp;
}

function get_yesterday_power($controller) {
    $key = "power{$controller}";
    $yesterday_return = initiate_api($key);
    return $yesterday_return;
}

function generate_graph_raw() {
    $key = "graph";
    initiate_api($key);
}


// Gathering yesterdays power from control panels
function get_yesterday_total() {
    $controllers = array("1", "2");
    $yesterday_total = 0;
    foreach ($controllers as $controller) {
	    $results = get_yesterday_power($controller);
	    $cf = fopen($results, 'r');
	    $yesterday = date("Y-m-d", strtotime("yesterday"));
	    $pattern = "/^{$yesterday}/";
	    while (!feof($cf)) {
		    $lines = fgets($cf);
		    if (preg_match($pattern, $lines)) {
			    $fields = explode(',' , $lines);
			    // strip first and last two fields
			    if ($controller == '1') {
				    $elements = array_slice($fields, 1);
				    $yesterday_total += array_sum($elements);
			    }
			    elseif ($controller == '2') {
				    $elements = array_slice($fields, 1, -2);
				    $yesterday_total += array_sum($elements);
                    return $yesterday_total;
			    }
		    }
	    }
    }
}



// XML node structure from API. Call it like an object
// ### If error points to this section of code, check cache files for valid data ###
//
$list_data = simplexml_load_file(get_list_data()) or die ("Unable to load XML string!");
$current = $list_data->System->DataSource;
$system_size = $list_data->System['PeakPower'];

// Get last data timestamp
$last_update_timestamp = $list_data->System->DataSource->LastData['Time'];

// Initiate counter for adding multiple fields from XML nodes
$current_system_output=0;
$current_energy_generated=0;
$lifetime_energy_generated=0;

// Adding together multiple XML nodes (dual controllers)
foreach ($current as $total) {
    $current_system_output += $total->LastData;
    $current_energy_generated += $total->Energy;
    $lifetime_energy_generated += $total->LifetimeEnergy;
}

// Initiate ad-hoc raw data collect for graphing
generate_graph_raw();

// HTML Callable Variables
// converting to kW
$current_system_output = ($current_system_output / 1000);	
$current_energy_generated = ($current_energy_generated / 1000);  
$lifetime_energy_generated = ($lifetime_energy_generated / 1000);

// Getting temp;
$system_temp = get_temp_data();

// Environmental Benefits Variables
$annual_co2 = ($lifetime_energy_generated / 1285 );
$gallon_of_gas = ($lifetime_energy_generated / 11.32 );
$barrels_of_oil = ($lifetime_energy_generated / 552.8 );

// Get yesterday total
$yesterday_total = (get_yesterday_total() / 1000);


?>

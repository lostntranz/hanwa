<?php

/*

API connect module for Tigo 
Do not make any changes to this file!
See config.php for configurable parameterss

*/

// Start declaring functions

function construct_uri($key) {
    require("config.php");
    $sysid = $sysid[$client];
    $today = date('Y-m-d');
    $yesterday = date("Y-m-d", strtotime("yesterday"));
    //
    if ($key == "list") {
        $cmd = $key;
        $uri_cmd = "cmd={$cmd}";
        return "{$uri_cmd}&sysid={$sysid}";
    }
    elseif ($key == "temp") {
        //https://datacenter.tigoenergy.com/api/data?cmd=split&date=2012-08-15&first=1&last=1&oids=465058
        //set api access to 'data'
        $cmd = "split"; 
        $first = "1";
        $last = "1";
        $oid = "$temp_oid[$client]";
        $uri_cmd = "cmd={$cmd}";
        return "{$uri_cmd}&first={$first}&last={$last}&oids={$oid}";
    }
    elseif ($key == "power1") {
        // https://datacenter.tigoenergy.com/api/data?cmd=data&sysid=5902&ds=04C05B801229&start=2012-08-16_00-00-00&end=2012-08-17_00-00-00&level=day&agg=sum

        $cmd = "data";
        $datasourceid = "$power1_dsid[$client]";
        $level = "day";
        $agg = "sum";
        $uri_cmd = "cmd={$cmd}&sysid={$sysid}&ds={$datasourceid}&start={$yesterday}_00-00-00&end={$today}_00-00-00&level={$level}&agg={$agg}";
        return $uri_cmd;
    }
    elseif ($key == "power2") {
        // https://datacenter.tigoenergy.com/api/data?cmd=data&sysid=5902&ds=04C05B8011FA&start=2012-08-16_00-00-00&end=2012-08-17_00-00-00&level=day&agg=sum

        $cmd = "data";
        $datasourceid = "$power2_dsid[$client]";
        $level = "day";
        $agg = "sum";
        $uri_cmd = "cmd={$cmd}&sysid={$sysid}&ds={$datasourceid}&start={$yesterday}_00-00-00&end={$today}_00-00-00&level={$level}&agg={$agg}";
        return $uri_cmd;
    }

    elseif ($key == "graph") {
        // https://datacenter.tigoenergy.com/api/data?cmd=data&sysid=5902&start=2012-08-20_11-00-00&end=2012-08-22_11-00-00&level=min&interval=60&oids=464103

        $cmd = "data";
        $moduleid = "$graph_oid[$client]";
        $graph_start_time = date("Y-m-d_H-00-00", strtotime("-2 day"));
        $graph_end_time = date("Y-m-d_H-00-00", strtotime("now"));
        $level = "min";
        $interval = "60";
        $uri_cmd = "cmd={$cmd}&sysid={$sysid}&start={$graph_start_time}&end={$graph_end_time}&level={$level}&interval={$interval}&oids={$moduleid}";
        return $uri_cmd;
    }

}

function get_data($key) {

    // Setting Tigo connect parameters
  $username = "Hanwha Solar";
    $password = "irvine1";
    // Main api site
    $remote_site = "https://datacenter.tigoenergy.com/api/data?";      	
    
    $uri = construct_uri($key);
    $url = "{$remote_site}{$uri}";
    $ch = curl_init();
    $timeout = 10;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $url);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC|CURLAUTH_ANYSAFE);
    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    $data = curl_exec($ch);
    //$status = curl_getinfo($ch);
    curl_close($ch);
    //return array($data, $status);
    return $data;

}

function check_cache($key, $cache_file) {
    if (file_exists($cache_file) && (time() - filemtime($cache_file) < 300 )) {
        return 1;
    }
    else {
        return 0;
    }
}

function initiate_api($key, $cache_dir=".") {
    $cache_file = "{$cache_dir}/cache/{$key}.cache";
    if (check_cache($key, $cache_file)) {
        return $cache_file;
    }
    else { 
        $cf = fopen($cache_file, 'w+');
        fwrite($cf, get_data($key));
        fclose($cf);
        return $cache_file;
    }
}

?>

